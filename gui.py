import bpy
from . operators import customCurveData
from . auto_load import force_register

# updater ops import, all setup in this file
from . import addon_updater_ops

class _ARPanel:
    bl_space_type = 'PROPERTIES'
    bl_region_type = 'WINDOW'
    bl_context = "material"

    @classmethod
    def poll(cls, context):
        return context.scene.render.engine in {'BLENDER_EEVEE', 'CYCLES'}

@force_register
class CHOCOFUR_PT_ui_angular_roughness(_ARPanel, bpy.types.Panel):
    bl_label = 'Chocofur Angular Roughness'

    def draw(self, context):
        properties = context.scene.chocofur_angular_roughness
        layout = self.layout
        col = layout.column(align=True)

        col.operator('material.set_angular_roughness', text='Low').option="LOW"
        col.operator('material.set_angular_roughness', text='Medium').option="MEDIUM"
        col.operator('material.set_angular_roughness', text='High').option="HIGH"
        col.operator('material.set_angular_roughness', text='Very High').option="VERY_HIGH"
        col.operator('material.set_angular_roughness', text='Custom').option="CUSTOM"

        col = layout.column()
        row = col.row(align=True)
        row.prop(properties, 'avoid_metals')
        if properties.avoid_metals:
            row.prop(properties, 'avoid_metals_treshold', slider=True, text="Treshold")
        
        col = layout.column()
        col.operator('material.remove_angular_roughness', text='Remove Effect', icon='CANCEL')

        row = layout.row()
        row.label(text='Scope')
        row.prop(properties, 'scope', expand=True)

@force_register
class CHOCOFUR_PT_ui_angular_roughness_custom_curve(_ARPanel, bpy.types.Panel):
    bl_label = 'Custom Angular Roughness Curve'
    bl_parent_id = 'CHOCOFUR_PT_ui_angular_roughness'
    
    def draw(self, context):
        properties = context.scene.chocofur_angular_roughness
        layout = self.layout
        col = layout.column(align=True)


        col.template_curve_mapping(customCurveData(), "mapping")
        col.operator('material.reset_custom_curve')

        layout.operator('material.copy_custom_curve_from_material')

############## Preferences ##############

# autoupdater preferences
class ChocofurManagerPreferences(bpy.types.AddonPreferences):
    bl_idname = __package__
   
    # addon updater preferences

    auto_check_update: bpy.props.BoolProperty(
        name="Auto-check for Update",
        description="If enabled, auto-check for updates using an interval",
        default=False,
        )
    
    updater_intrval_months: bpy.props.IntProperty(
        name='Months',
        description="Number of months between checking for updates",
        min=0,
        default=0,
        )
        
    updater_intrval_days: bpy.props.IntProperty(
        name='Days',
        description="Number of days between checking for updates",
        min=0,
        max=31,
        default=7,
        )
    
    updater_intrval_hours: bpy.props.IntProperty(
        name='Hours',
        description="Number of hours between checking for updates",
        min=0,
        max=23,
        default=0,
        )
    
    updater_intrval_minutes: bpy.props.IntProperty(
        name='Minutes',
        description="Number of minutes between checking for updates",
        min=0,
        max=59,
        default=0,
        )
    
    def draw(self, context):    
        layout = self.layout
        # col = layout.column() # works best if a column, or even just self.layout
        mainrow = layout.row()
        col = mainrow.column()

        # updater draw function
        # could also pass in col as third arg
        addon_updater_ops.update_settings_ui(self, context)

        # Alternate draw function, which is more condensed and can be
        # placed within an existing draw function. Only contains:
        #   1) check for update/update now buttons
        #   2) toggle for auto-check (interval will be equal to what is set above)
        # addon_updater_ops.update_settings_ui_condensed(self, context, col)

        # Adding another column to help show the above condensed ui as one column
        # col = mainrow.column()
        # col.scale_y = 2
        # col.operator("wm.url_open","Open webpage ").url=addon_updater_ops.updater.website