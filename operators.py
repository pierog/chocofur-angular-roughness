import bpy

from bpy.props import PointerProperty, EnumProperty, BoolProperty, FloatProperty
from mathutils import Vector

class Chocofur_Angular_Roughness_Properties(bpy.types.PropertyGroup):
    scope: EnumProperty(
        name="Scope",
        description="Scope of affected materials",
        items = [
                ("MATERIAL", "Material", "", 0),
                ("OBJECT", "Object", "", 1),
                ("SCENE", "Scene", "", 2),
            ],
        default = "MATERIAL",
        )
    avoid_metals: BoolProperty(
        name="Avoid Metals",
        default=False
    )
    avoid_metals_treshold: FloatProperty(
        name="Avoid Metals Treshold",
        default=0,
        min=0,
        max=1,
    )

class _AROperator:
    @classmethod
    def poll(cls, context):
        props = context.scene.chocofur_angular_roughness
        if props.scope in ('MATERIAL', 'OBJECT'):
            return context.object and context.object.active_material and context.scene.render.engine in {'BLENDER_EEVEE', 'CYCLES'}
        else:
            return context.scene and context.scene.render.engine in {'BLENDER_EEVEE', 'CYCLES'}

class CHOCOFUR_OT_RemoveAngularRoughness(_AROperator, bpy.types.Operator):
    bl_idname = "material.remove_angular_roughness"
    bl_label = "Remove Angular Roughness"

    def execute(self, context):
        props = context.scene.chocofur_angular_roughness

        if props.scope == 'MATERIAL':
            node_tree = context.object.active_material.node_tree
            remove_from_material(node_tree)
        elif props.scope == 'OBJECT':
            materials = {m.material for m in context.object.material_slots if m.material != None}
            for m in materials:
                remove_from_material(m.node_tree)
        elif props.scope == 'SCENE':
            materials = {m.material for ob in context.scene.objects for m in ob.material_slots if m.material != None}
            for m in materials:
                remove_from_material(m.node_tree)

        return {'FINISHED'}

def remove_from_material(node_tree):
    layerweights = {n for n in node_tree.nodes if "Angular Roughness Layer Weight" in n.name}
    rgbcurves = {n for n in node_tree.nodes if "Angular Roughness Curve" in n.name}
    mixes = {n for n in node_tree.nodes if "Angular Roughness Mix" in n.name}
    groups = {n for n in node_tree.nodes if n.type == 'GROUP'}

    for n in layerweights | rgbcurves:
        node_tree.nodes.remove(n)
    
    for n in mixes:
        from_socket = n.inputs['Color1'].links[0].from_socket
        to_sockets = [s.to_socket for s in n.outputs['Color'].links]
        
        node_tree.nodes.remove(n)

        for to_socket in to_sockets:
            node_tree.links.new(from_socket, to_socket)

    for g in groups:
        remove_from_material(g.node_tree)

class CHOCOFUR_OT_SetAngularRoughness(_AROperator, bpy.types.Operator):
    bl_idname = "material.set_angular_roughness"
    bl_label = "Set Angular Roughness"
    option: EnumProperty(
        name="Option",
        items = [
                ("LOW", "Low", "", 0),
                ("MEDIUM", "MEDIUM", "", 1),
                ("HIGH", "High", "", 2),
                ("VERY_HIGH", "Very High", "", 3),
                ("CUSTOM", "Custom", "", 4),
            ],
        default = "MEDIUM",
        )

    def execute(self, context):
        props = context.scene.chocofur_angular_roughness

        if props.scope == 'MATERIAL':
            self.adjust_material(props, context.object.active_material)
        elif props.scope == 'OBJECT':
            materials = {m.material for m in context.object.material_slots if m.material != None}
            for m in materials:
                self.adjust_material(props, m)
        elif props.scope == 'SCENE':
            materials = {m.material for ob in context.scene.objects for m in ob.material_slots if m.material != None}
            for m in materials:
                self.adjust_material(props, m)

        return {'FINISHED'}

    def adjust_material(self, props, material):
        node_tree = material.node_tree
        output_node = node_tree.get_output_node('ALL')
        try:
            status, node, bsdf, node_tree, output_node = find_best_node(output_node, node_tree, output_node)
        except MaterialNotCompatibleException:
            self.report({'INFO'}, f"{material.name}: Please use Principled BSDF shader as the Material Output")
            return


        links = node_tree.links

        if props.avoid_metals and bsdf.inputs['Metallic'].default_value > props.avoid_metals_treshold:
            remove_from_material(node_tree)
            return

        if status == 'CURVE':
            self.adjustCurve(node)
        elif status == 'BSDF_CLEAR':
            roughness_socket = bsdf.inputs['Roughness']

            start_location = bsdf.location
            offset = Vector((bsdf.dimensions.x + 10, 0))

            layerweight = layerweight_factory(node_tree)
            layerweight.location = start_location
            layerweight.location.y -= 220

            rgbcurves = rgbcurves_factory(node_tree)
            rgbcurves.location = start_location + offset
            rgbcurves.location.y -= bsdf.dimensions.y*.4

            bsdf.location += 2*offset
            output_node.location += 2*offset

            self.adjustCurve(rgbcurves)
            
            links.new(rgbcurves.outputs['Color'], roughness_socket)
            links.new(layerweight.outputs['Facing'], rgbcurves.inputs['Color'])
        elif status == 'BSDF_INTERCEPT':
            bsdf = node
            roughness_socket = bsdf.inputs['Roughness']
            intercepted_socket = roughness_socket.links[0].from_socket

            start_location = bsdf.location
            offset = Vector((bsdf.dimensions.x + 10, 0))

            layerweight = layerweight_factory(node_tree)
            layerweight.location = start_location
            layerweight.location.y -= bsdf.dimensions.y*.4

            rgbcurves = rgbcurves_factory(node_tree)
            rgbcurves.location = start_location + offset
            rgbcurves.location.y -= bsdf.dimensions.y*.4

            mixnode = mixnode_factory(node_tree)
            mixnode.location = start_location + 2*offset
            mixnode.location.y -= bsdf.dimensions.y*.4

            bsdf.location += 3*offset
            output_node.location += 3*offset

            self.adjustCurve(rgbcurves)
            
            links.new(layerweight.outputs['Facing'], rgbcurves.inputs['Color'])
            links.new(rgbcurves.outputs['Color'], mixnode.inputs['Color2'])
            links.new(mixnode.outputs['Color'], roughness_socket)
            links.new(intercepted_socket, mixnode.inputs['Color1'])

    def adjustCurve(self, rgbcurves):
        rgbcurves.mapping.tone="STANDARD"
        C_curve = rgbcurves.mapping.curves[3]

        # reset curve
        for i in range(len(C_curve.points)-2):
            C_curve.points.remove(C_curve.points[i])
        C_curve.points.new(.5, .5)

        if self.option == 'LOW':
            C_curve.points[0].location = Vector((0, 1))
            C_curve.points[1].location = Vector((.75, .75))
            C_curve.points[2].location = Vector((1, 0))
        elif self.option == 'MEDIUM':
            C_curve.points[0].location = Vector((0, .75))
            C_curve.points[1].location = Vector((.75, .5))
            C_curve.points[2].location = Vector((1, 0))
        elif self.option == 'HIGH':
            C_curve.points[0].location = Vector((0, .5))
            C_curve.points[1].location = Vector((.75, .25))
            C_curve.points[2].location = Vector((1, 0))
        elif self.option == 'VERY_HIGH':
            C_curve.points[0].location = Vector((0, .25))
            C_curve.points[1].location = Vector((.75, .125))
            C_curve.points[2].location = Vector((1, 0))
        elif self.option == 'CUSTOM':
            custom_curve = customCurveData()
            copy_curve(custom_curve.mapping.curves[3], C_curve)

        rgbcurves.mapping.update()

def layerweight_factory(node_tree):
    layerweight = node_tree.nodes.new('ShaderNodeLayerWeight')
    layerweight.name = "Angular Roughness Layer Weight"
    layerweight.label = "Angular Roughness Layer Weight"
    layerweight.color = (.922, .363, 0)
    layerweight.use_custom_color = True
    return layerweight

def rgbcurves_factory(node_tree):
    rgbcurves = node_tree.nodes.new('ShaderNodeRGBCurve')
    rgbcurves.name = "Angular Roughness Curve"
    rgbcurves.label = "Angular Roughness Curve"
    rgbcurves.color = (.922, .363, 0)
    rgbcurves.use_custom_color = True
    return rgbcurves

def mixnode_factory(node_tree):
    mixnode = node_tree.nodes.new('ShaderNodeMixRGB')
    mixnode.name = "Angular Roughness Mix"
    mixnode.label = "Angular Roughness Mix"
    mixnode.inputs['Fac'].default_value = 1
    mixnode.blend_type = 'MULTIPLY'
    mixnode.color = (.922, .363, 0)
    mixnode.use_custom_color = True
    return mixnode

class MaterialNotCompatibleException(Exception): pass

class BestNode:
    def __init__(self, status, node, closest_bsdf, node_tree, output_node):
        self.status = status
        self.best_node = node
        self.closest_bsdf = closest_bsdf
        self.node_tree = node_tree
        self.output_node = output_node

def find_best_node(node, node_tree, output_node, socket_id=None):
    # return: status, best_node, closest_bsdf, node_tree, output_node
    try:
        if node.type == 'GROUP':
            for n in node.node_tree.nodes:
                if n.type == 'GROUP_OUTPUT':
                    input_socket = n.inputs[socket_id].links[0]
                    from_socket_id = input_socket.from_node.outputs.values().index(input_socket.from_socket)
                    return find_best_node(input_socket.from_node, node.node_tree, n, from_socket_id)
        elif node.type == 'BSDF_PRINCIPLED':
            roughness_socket = node.inputs['Roughness']
            if roughness_socket.links:
                if roughness_socket.links[0].from_node.type == 'CURVE_RGB' and "Angular Roughness Curve" in roughness_socket.links[0].from_node.name:
                    return ("CURVE", roughness_socket.links[0].from_node, node, node_tree, output_node)
                elif roughness_socket.links[0].from_node.type == 'MIX_RGB' and "Angular Roughness Mix" in roughness_socket.links[0].from_node.name:
                    return find_best_node(roughness_socket.links[0].from_node, node_tree, output_node)
                else:
                    return ("BSDF_INTERCEPT", node, node, node_tree, output_node)
            else:
                return ("BSDF_CLEAR", node, node, node_tree, output_node)
            pass
        elif node.type == 'MIX_RGB' and "Angular Roughness Mix" in node.name:
            if node.inputs['Color2'].links[0].from_node.type == 'CURVE_RGB' and "Angular Roughness Curve" in node.inputs['Color2'].links[0].from_node.name:
                bsdf = [l.to_node for l in node.outputs['Color'].links if l.to_node.type == 'BSDF_PRINCIPLED'][0]
                return ("CURVE", node.inputs['Color2'].links[0].from_node, bsdf, node_tree, output_node)
        elif node.type == 'OUTPUT_MATERIAL':
            input_socket = node.inputs[0].links[0]
            from_socket_id = input_socket.from_node.outputs.values().index(input_socket.from_socket)
            return find_best_node(input_socket.from_node, node_tree, output_node, from_socket_id)
    except:
        raise MaterialNotCompatibleException
        
    raise MaterialNotCompatibleException


def newNodeTree(curve_name):
    if curve_name not in bpy.data.node_groups:
        bpy.data.node_groups.new(curve_name, 'ShaderNodeTree')
    return bpy.data.node_groups[curve_name].nodes

curve_node_mapping = {}
def customCurveData(curve_name = "AngularRoughnessCustomCurveData"):
    def node_tree_and_curve():
        nt = newNodeTree(curve_name)
        try:
            if nt.nodes[0].type == 'CURVE_RGB':
                curve_node_mapping[curve_name] = nt.nodes[0].name
        except:
            cn = newNodeTree(curve_name).new('ShaderNodeRGBCurve')
            curve_node_mapping[curve_name] = cn.name

    if curve_name not in curve_node_mapping:
        node_tree_and_curve()    
    try:
        return newNodeTree(curve_name)[curve_node_mapping[curve_name]]
    except KeyError:
        # eg. after opening new file curve_node_mapping won't be empty but node group won't exist
        node_tree_and_curve()
        return newNodeTree(curve_name)[curve_node_mapping[curve_name]]


def copy_curve(from_curve, to_curve):
    # reset curve
    for i in range(len(to_curve.points)-2):
        to_curve.points.remove(to_curve.points[i])

    # match length
    for i in range(len(from_curve.points)-2):
        to_curve.points.new(.5, .5)

    for i in range(len(from_curve.points)):
        to_curve.points[i].location = from_curve.points[i].location
        
class CHOCOFUR_OT_CopyCustomCurveFromMaterial(_AROperator, bpy.types.Operator):
    '''Copy Angular Roughness Curve from Material as Custom'''
    bl_idname = "material.copy_custom_curve_from_material"
    bl_label = "Copy Curve from Material"

    def execute(self, context):
        material = context.active_object.active_material
        node_tree = material.node_tree
        output_node = node_tree.get_output_node('ALL')
        try:
            status, curve_node, bsdf, node_tree, output_node = find_best_node(output_node, node_tree, output_node)
            if status != 'CURVE':
                raise MaterialNotCompatibleException
        except MaterialNotCompatibleException:
            self.report({'INFO'}, f"{material.name}: Please use Principled BSDF shader as the Material Output")
            return

        cn = customCurveData()
        copy_curve(curve_node.mapping.curves[3], cn.mapping.curves[3])
        cn.mapping.update()

        return {'FINISHED'}

class CHOCOFUR_OT_ResetCustomCurve(_AROperator, bpy.types.Operator):
    '''Reset Angular Roughness Custom Curve'''
    bl_idname = "material.reset_custom_curve"
    bl_label = "Reverse-Slope Shape [ \ ]"

    def execute(self, context):
        cn = customCurveData()
        curve = cn.mapping.curves[3]
        # reset curve
        for i in range(len(curve.points)-2):
            curve.points.remove(curve.points[i])
        curve.points[0].location.x = 0
        curve.points[0].location.y = 1
        curve.points[1].location.x = 1
        curve.points[1].location.y = 0
        cn.mapping.update()

        return {'FINISHED'}

def register():
    bpy.types.Scene.chocofur_angular_roughness = PointerProperty(type=Chocofur_Angular_Roughness_Properties)

def unregister():
    del bpy.types.Scene.chocofur_angular_roughness